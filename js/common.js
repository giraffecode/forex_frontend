app.directive('loading', ['$http', function ($http) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };
      scope.$watch(scope.isLoading, function (value) {
        if(value){
          element.removeClass('ng-hide');
        }else{
          element.addClass('ng-hide');
        }
      });
    }
  };
}]);

app.directive('loaded', ['$http', function ($http) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };
      scope.$watch(scope.isLoading, function (value) {
        if(value){
          element.addClass('ng-hide');
          element.removeClass('ng-show');
        }else{
          element.removeClass('ng-hide');
          element.addClass('ng-show');
        }
      });
    }
  };
}]);

app.service('SrvCatgeory',['$http',function($http){
  var service = {
    ListCategory:function(){
      return $http.get('https://forex-steps.com/public/api/listCategory').success(function(response){})
    }
  }
  return service;
}])

app.service('SrvImportantNews',['$http',function($http){
  var service = {
    ImportantNews:function(){
      return $http.get('https://forex-steps.com/public/api/getImportantNews').success(function(response){})
    }
  }
  return service;
}])

app.service('SrvGetLatestNews',['$http',function($http){
  var service = {
    GetLatestNews:function(){
      return $http.get('https://forex-steps.com/public/api/getLatestNews').success(function(response){})
    }
  }
  return service;
}])

app.service('SrvNewsDetails',['$http',function($http){
  var service = {
    NewsDetails:function(id){
      return $http.get('https://forex-steps.com/public/api/newsDetails/'+id).success(function(response){})
    }       
  }
  return service;
}])

app.service('SrvCategoriesAndNews',['$http',function($http){
  var service = {
    CategoriesAndNews:function(){
      return $http.get('https://forex-steps.com/public/api/categoriesAndNews').success(function(response){})
    },
    RelatedNewsCategory:function(id){
      return $http.get('https://forex-steps.com/public/api/relatedNewsCategory/'+id).success(function(response){})
    },
    RelatedNewsCategoryPagination:function(category_id, pageNumber){
      return $http.get('https://forex-steps.com/public/api/relatedNewsCategory/'+category_id+'?page='+pageNumber).success(function(response){})
    }
  }
  return service;
}])